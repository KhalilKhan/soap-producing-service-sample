package com.example.producingwebservice;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Repository;
import io.spring.guides.gs_producing_web_service.Country;
import io.spring.guides.gs_producing_web_service.Currency;

@Repository
public class CountryRepository {
    private static final Map<String, Country> COUNTRIES_MAP = new HashMap<String,Country>();
    private static final Country NA_COUNTRY = new Country();

    @PostConstruct
	public void initData() {
        NA_COUNTRY.setName("XXX");
		NA_COUNTRY.setCapital("XXX");
		NA_COUNTRY.setCurrency(Currency.XXX);
		NA_COUNTRY.setPopulation(-1);
	}

	public Country findCountry(String name) {
		return COUNTRIES_MAP.getOrDefault(name, NA_COUNTRY);
	}

	public Country addCountry(String name, String capital, Currency currency, int population) {
		Country country = new Country();
		country.setName(name);
		country.setCapital(capital);
		country.setCurrency(currency);
		country.setPopulation(population);
		return COUNTRIES_MAP.putIfAbsent(name, country);
	}

}
